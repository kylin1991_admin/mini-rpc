package org.example.provider;

import org.example.api.ITestService;

public class TestServiceImpl implements ITestService {
    @Override
    public String sayHello(String content) {
        System.out.println("RPC request in :" + content);
        return "Success";
    }
}
