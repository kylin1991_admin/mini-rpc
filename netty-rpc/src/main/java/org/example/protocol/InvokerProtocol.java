package org.example.protocol;

import lombok.Data;

import java.io.Serializable;

@Data
public class InvokerProtocol implements Serializable {
    private String className;
    private String methodName;
    // 形参列表
    private Class<?>[] parames;
    // 实参列表
    private Object[] values;
}
