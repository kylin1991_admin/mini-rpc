package org.example.v1.provider;

import org.example.v1.api.IHelloService;

public class HelloServiceImpl implements IHelloService {
    @Override
    public String sayHello(String content) {
        System.out.println("request in： " + content);
        return "SUCCESS";
    }
}
