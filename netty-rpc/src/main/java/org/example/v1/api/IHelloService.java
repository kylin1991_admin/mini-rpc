package org.example.v1.api;

public interface IHelloService {
    String sayHello(String content);
}
