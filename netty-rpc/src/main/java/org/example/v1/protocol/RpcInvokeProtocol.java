package org.example.v1.protocol;

import lombok.Data;

import java.io.Serializable;

@Data
public class RpcInvokeProtocol implements Serializable {
    private String className;
    private String methodName;
    private Class<?>[] params;
    private Object[] values;
}
