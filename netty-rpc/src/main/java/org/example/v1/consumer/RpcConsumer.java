package org.example.v1.consumer;

import org.example.v1.api.IHelloService;
import org.example.v1.consumer.proxy.RpcProxy;

public class RpcConsumer {
    public static void main(String[] args) {
        IHelloService helloService = RpcProxy.create(IHelloService.class);
        System.out.println(helloService.sayHello("lilei"));

    }
}
