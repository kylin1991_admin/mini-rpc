package org.example.consumer;

import org.example.api.ITestService;
import org.example.consumer.proxy.RpcProxy;

public class RpcConsumer {
    public static void main(String[] args) {
        ITestService service = RpcProxy.create(ITestService.class);
        System.out.println(service.sayHello("你好"));
    }
}
