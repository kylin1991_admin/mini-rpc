package org.example.consumer.proxy;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;
import org.example.protocol.InvokerProtocol;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class RpcProxy {
    public static <T> T create(Class<T> interfaceCls) {
        return (T) Proxy.newProxyInstance(interfaceCls.getClassLoader(), new Class<?>[]{interfaceCls}, new MethodProxy());
    }

    public static class MethodProxy implements InvocationHandler {
        // 可以直接读取配置文件
        private static final String HOST = "localhost";
        private static final int PORT = 8080;

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            if (Object.class.equals(method.getDeclaringClass())) {
                // 类就走这个逻辑，直接调用自己的方法返回
                return method.invoke(this, args);
            } else {
                // 接口就走这个逻辑
                return RpcInvoker(proxy, method, args);
            }
        }

        private Object RpcInvoker(Object proxy, Method method, Object[] args) {
            InvokerProtocol invokerProtocol = new InvokerProtocol();
            invokerProtocol.setClassName(method.getDeclaringClass().getName());
            invokerProtocol.setMethodName(method.getName());
            invokerProtocol.setParames(method.getParameterTypes());
            invokerProtocol.setValues(args);

            final RpcProxyHandler rpcProxyHandler = new RpcProxyHandler();

            EventLoopGroup workerGroup = new NioEventLoopGroup();

            Bootstrap client = new Bootstrap();
            client.group(workerGroup)
                    .channel(NioSocketChannel.class)
                    .option(ChannelOption.SO_KEEPALIVE, true)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            ChannelPipeline pipeline = ch.pipeline();
                            pipeline.addLast(new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4, 0, 4));
                            pipeline.addLast(new LengthFieldPrepender(4));

                            pipeline.addLast("decoder", new ObjectDecoder(Integer.MAX_VALUE, ClassResolvers.cacheDisabled(null)));
                            pipeline.addLast("encoder", new ObjectEncoder());

                            pipeline.addLast(rpcProxyHandler);
                        }
                    });

            try {
                ChannelFuture f = client.connect(HOST, PORT).sync();
                f.channel().writeAndFlush(invokerProtocol).sync();
                f.channel().closeFuture().sync();

            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                workerGroup.shutdownGracefully();
            }

            return rpcProxyHandler.getResultData();
        }
    }
}

