package org.example.consumer.proxy;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.SneakyThrows;

public class RpcProxyHandler extends ChannelInboundHandlerAdapter {
    private Object resultData;

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        resultData = msg;
    }

    @SneakyThrows
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        System.out.println("client exception : " + cause.getMessage());
        super.exceptionCaught(ctx, cause);
    }

    public Object getResultData() {
        return resultData;
    }
}
